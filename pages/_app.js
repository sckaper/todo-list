import "../styles/globals.css";
import { useState, createContext } from "react";
import addTodoList from "../components/addTodoList";
import modifyTodoList from "../components/modifyTodoList";
import * as yup from "yup";

const context = createContext();

const App = ({ Component, pageProps }) => {
  const initialValues = {
    title: "",
  };

  const validationSchema = yup.object().shape({
    title: yup.string().required(),
  });

  const [todoListCreatePage, setTodoListCreatePage] = useState(false);
  const [todoListModifyPage, setTodoListModifyPage] = useState(false);
  const [todoLists, setTodoLists] = useState([]);
  const [currentId, setCurrentId] = useState(-1);
  const [idSelected, setIdSelected] = useState(0);

  const todoListStartId = "todoList";
  const todoStartId = "todo";

  const todoListCreateSubmit = (values, { resetForm }) => {
    addTodoList(String(values.title), setTodoLists, setCurrentId, currentId);
    setTodoListCreatePage(false);
    resetForm({ values: "" });
  };

  const todoListModifySubmit = (values, { resetForm }) => {
    modifyTodoList(idSelected, String(values.title), todoLists);
    setTodoListModifyPage(false);
    resetForm({ values: "" });
  };

  return (
    <context.Provider
      value={{
        todoLists,
        setTodoLists,
        currentId,
        setCurrentId,
        idSelected,
        setIdSelected,
        todoListStartId,
        todoStartId,
        todoListCreatePage,
        setTodoListCreatePage,
        todoListModifyPage,
        setTodoListModifyPage,
        initialValues,
        todoListCreateSubmit,
        todoListModifySubmit,
        validationSchema,
      }}
    >
      <Component {...pageProps} />
    </context.Provider>
  );
};

export default App;
export { context };
