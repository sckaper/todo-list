import { useContext } from "react";
import { context } from "../pages/_app";
import Button from "./CreateButton";

const ListTodoLists = () => {
  const { todoLists, todoListStartId, setIdSelected } = useContext(context);
  return (
    <div className="flex">
      {todoLists.map((item) => (
        <Button
          className="border rounded-t-xl px-3 py-2 border-solid border-gray-300 border-b-transparent"
          id={todoListStartId + `${+item.id}`}
          key={item.id}
          onClick={() => setIdSelected(item.id)}
          text={item.title}
        />
      ))}
    </div>
  );
};

export default ListTodoLists;
