const modifyTodoList = (id, newTitle, todoLists) => {
  console.log(newTitle);
  console.log(
    todoLists
      .filter((todoList) => Number(todoList.id) === id)
      .map((todoList) => (todoList.title = newTitle))
  );
};

export default modifyTodoList;
