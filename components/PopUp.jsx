import { Formik, Form, Field } from "formik";
import { useContext } from "react";
import { context } from "../pages/_app";
import Button from "./CreateButton";

const PopUp = (props) => {
  const { initialValues, validationSchema } = useContext(context);

  const { page, setPage, text, subText, submit } = props;
  return (
    <div className={page ? "visible" : "invisible"}>
      <div className="absolute bottom-0 h-screen w-screen bg-white rounded-xl">
        <div className="p-4">
          <header className="flex justify-between border border-solid border-transparent border-b-gray-300">
            <p className="font-bold text-xl">{text}</p>
            <p>⛌</p>
          </header>
          <p className="font-bold">{subText}</p>

          <Formik
            initialValues={initialValues}
            onSubmit={submit}
            validationSchema={validationSchema}
          >
            <Form>
              <Field
                type="text"
                className="border border-solid border-gray-300 rounded"
                name="title"
              />
            </Form>
          </Formik>
        </div>
        <Button text="close Popup" onClick={() => setPage} />
      </div>
    </div>
  );
};

export default PopUp;
