import increaseID from "./increaseId";

const addTodoList = (title, setTodoLists, setCurrentId, currentId) => {
  increaseID(currentId, setCurrentId);
  setTodoLists((todoList) => [
    ...todoList,
    {
      id: currentId,
      title: String(title),
      todos: [{ id: currentId, title: String(title) }],
    },
  ]);
};

export default addTodoList;
